// ======================================================================== //
// Copyright 2020-2020 The Authors                                          //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "owl/owl.h"
#include "owl/common/math/vec.h"
#include <vector>
#include <map>
#include <stdexcept>
#include <string>
#include <iostream>
#include <memory>
#include "hdri.h"
#include "mesh.h"

namespace cdf {

  using namespace owl;
  using namespace owl::common;

  enum PixelFormat { PF_RGB32F, PF_RGBA32F };
 
  struct CDF {
    typedef std::shared_ptr<CDF> SP;
    
    virtual ~CDF();

    static CDF::SP load(const std::string &hdrFileName);

    Mesh::SP asTriangleMesh(Mesh::Representation repr,
                            unsigned maxQuads,
                            bool dumpAsObj = false);
                           

    // Containing *normalized* prefix sums of rows
    std::vector<float> cumulatedRows;

    // The last column, cumulated and normalized
    std::vector<float> cumulatedLastCol;

    // The original environment map
    HDRI hdri;

  };
  
}
