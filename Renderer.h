// ======================================================================== //
// Copyright 2020-2020 The Authors                                          //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "owl/owl.h"
#include "owl/common/math/box.h"
#include "owl/common/math/vec.h"
#include <cuda_runtime.h>

namespace cdf {

  using namespace owl;
  using namespace owl::common;

  enum RenderMode {
    RenderModeBinarySearch,
    RenderModeBVH,
  };

  enum BenchmarkMode {
    BenchmarkModeBinarySearch,
    BenchmarkModeBVH,
    BenchmarkModeErrors,
    BenchmarkModeNone,
  };

  struct Renderer {
    Renderer(std::string hdrFileName, std::string objFileName);

    void setCamera(const vec3f &org,
                   const vec3f &dir_00,
                   const vec3f &dir_du,
                   const vec3f &dir_dv);
    void render(const vec2i &fbSize,
                uint32_t *fbPointer);
    void beginErrorBenchmark();
    void endErrorBenchmark();

    OWLParams  lp;
    OWLRayGen  rayGenRender;
    OWLRayGen  rayGenBenchmark;
    OWLContext owl;
    OWLModule  module;

    struct Texture {
      cudaArray_t pixelArray;
      cudaTextureObject_t texObj;
    };
    Texture hdrTexture;

    OWLBuffer environmentMapRows;
    OWLBuffer environmentMapCols;

    OWLBuffer offSamples;
    OWLBuffer offLuminance;

    OWLGeomType triangleGeomType;
    OWLGeomType modelGeomType;
    OWLGroup cdfRowGroup;
    OWLGroup cdfLastColGroup;
    OWLGroup modelGroup;

    OWLBuffer accumBuffer { 0 };
    int accumID { 0 };

    void resetAccum() { accumID = 0; }
    
    vec2i      fbSize { 1 };

    box3f modelBounds;
    
    static int   spp;
    static bool  heatMapEnabled;
    static float heatMapScale;
    static RenderMode renderMode;
    static BenchmarkMode benchmarkMode;
    static int offSamplesMax;
    static bool cdfDumpAsObj;
    static unsigned maxControlPoints;
  };
  
} // ::cdf
