#include <stdint.h>
#include <functional>
#include <limits.h>

namespace cdf{

struct Rectangle {
    uint32_t width;
    uint32_t height;
    uint32_t x;
    uint32_t y;
    bool undefined = true;

    Rectangle() {
      undefined = true;
    }

    Rectangle(uint32_t width, uint32_t height, uint32_t x, uint32_t y) {
      this->undefined = false;
      this->width = width;
      this->height = height;
      this->x = x;
      this->y = y;
    }

    bool consecutiveTo(Rectangle &other) {
      if (other.undefined)
        return false;

      // consecutive in Y
      if (this->x == other.x && (this->y == (other.y + other.height) || (this->y + this->height) == other.y) && this->width == other.width)
        return true;

      // consecutive in X
      if (this->y == other.y && (this->x == (other.x + other.width) || (this->x + this->width) == other.x) && this->height == other.height)
        return true;

      return false;
    }

    Rectangle merge(Rectangle &other) {
      if (this->x == other.x && (this->y == (other.y + other.height) || (this->y + this->height) == other.y) && this->width == other.width)
        return Rectangle(this->width, this->height + other.height, min(this->x, other.x), min(this->y, other.y));
      
      if (this->y==other.y && (this->x == (other.x + other.width) || (this->x + this->width) == other.x) && this->height==other.height)
        return Rectangle(this->width + other.width, this->height, min(this->x, other.x), min(this->y, other.y));
      
      // undefined
      return Rectangle();
    }

    void forEachPoint(std::function<void(uint32_t, uint32_t)> callback) {
      for (uint32_t x0 = 0; x0 < this->width; ++x0)
        for (uint32_t y0 = 0; y0 < this->height; ++y0)
          callback(this->x + x0, this->y + y0);
    }

    float getVal(uint32_t x, uint32_t y, float* image, uint32_t width, uint32_t height) {
        uint32_t x_ = this->x + x;
        uint32_t y_ = this->y + y;
        uint32_t addr = x_ + y_ * width;
        return image[addr];
    }

    float getMinRightVal(float* image, uint32_t width, uint32_t height) {
        float minRightVal = std::numeric_limits<float>::max();
        uint32_t x = this->x + this->width - 1;
        for (uint32_t y = 0; y < this->height; ++y) {
            uint32_t addr = x + y * width;
            minRightVal = min(image[addr], minRightVal);
        }
        return minRightVal;
    }

    float getMaxRightVal(float* image, uint32_t width, uint32_t height) {
        float maxRightVal = std::numeric_limits<float>::min();
        uint32_t x = this->x + this->width - 1;
        for (uint32_t y = 0; y < this->height; ++y) {
            uint32_t addr = x + y * width;
            maxRightVal = max(image[addr], maxRightVal);
        }
        return maxRightVal;
    }

    float getMinLeftVal(float* image, uint32_t width, uint32_t height) {
        float minLeftVal = std::numeric_limits<float>::max();
        int x = (this->x - 1);
        if (x < 0) return 0.f;
        for (uint32_t y = 0; y < this->height; ++y) {
            uint32_t addr = x + y * width;
            minLeftVal = min(image[addr], minLeftVal);
        }
        return minLeftVal;
    }

    float getMaxLeftVal(float* image, uint32_t width, uint32_t height) {
        float maxLeftVal = std::numeric_limits<float>::min();
        int x = (this->x - 1);
        if (x < 0) return 0.f;
        for (uint32_t y = 0; y < this->height; ++y) {
            uint32_t addr = x + y * width;
            maxLeftVal = max(image[addr], maxLeftVal);
        }
        return maxLeftVal;
    }
  };

};
