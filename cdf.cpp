// ======================================================================== //
// Copyright 2020-2020 The Authors                                          //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include <algorithm>
#include <cassert>
#include <cfloat>
#include <cstring>
#include <fstream>
#include <sstream>
#include <random>

#include "cdf.h"

#include "rectangle.h"

#define DUMP_CDF 1
#define DUMP_OBJ 0

namespace cdf {

  template <typename InIt, typename OutIt>
  void scan(InIt first, InIt last, OutIt dest, unsigned stride = 1)
  {
    for (ptrdiff_t i = 0; i != last-first; i += stride)
    {
      *(dest + i) = i == 0 ? *first : *(dest + i - stride) + *(first + i);
    }
  }

  template <typename It>
  void normalize(It first, It last)
  {
    // Assumes that [first,last) is sorted!
    auto bck = *(last-1);
    if (bck != 0)
    {
      for (It it = first; it != last; ++it)
      {
        *it /= bck;
      }
    }
  }
  
  void makeCDF(void const* imgData, unsigned numComponents, int width, int height,
               std::vector<float> &cumulatedRows, std::vector<float> &cumulatedLastCol)
  {
    // Build up luminance image
    std::vector<float> luminance(width * height);
  
    struct vec3 { float x, y, z; };
    struct vec4 { float x, y, z, w; };

    for (int y = 0; y < height; ++y)
    {
      for (int x = 0; x < width; ++x)
      {
        // That's not actually luminance, but might as well be..
        if (numComponents == 3)
        {
          vec3 rgb = *((vec3*)imgData + y * width + x);
          luminance[y * width + x] = max(rgb.x,max(rgb.y,rgb.z));
        }
        else if (numComponents == 4)
        {
          vec4 rgba = *((vec4*)imgData + y * width + x);
          luminance[y * width + x] = max(rgba.x,max(rgba.y,rgba.z));
        }
        else
          assert(0);
      }
    }
 

    #define NORMALIZE_ROWS

    // Build up CDF
    cumulatedRows.resize(width * height, 0);
    cumulatedLastCol.resize(height);
    std::vector<float> lastCol(height);

    for (int y = 0; y < height; ++y)
    {
      // Scan each row
      size_t off = y * width;
      scan(luminance.data() + off, luminance.data() + off + width, cumulatedRows.data() + off);
      // Assemble the last column by filling with the last item of each row
      lastCol[y] = *(cumulatedRows.data() + off + width - 1);

      #ifdef NORMALIZE_ROWS
      // Normalize the row
      normalize(cumulatedRows.data() + off, cumulatedRows.data() + off + width);
      #endif
    }

    // Scan and normalize the last column
    scan(lastCol.begin(), lastCol.end(), cumulatedLastCol.begin());

    #ifndef NORMALIZE_ROWS
    // Normalize the whole image
    float mx = cumulatedLastCol[cumulatedLastCol.size() - 1];
    for (int y = 0; y < height; ++y) {
      for (int x = 0; x < width; ++x) {
        cumulatedRows[x + y * width] /= mx;
      }
    }
    #endif 
    normalize(cumulatedLastCol.begin(), cumulatedLastCol.end());
  }

  CDF::~CDF() {}

  CDF::SP CDF::load(const std::string &hdrFileName) {

    CDF::SP res = std::make_shared<CDF>();

    res->hdri.load(hdrFileName);

    makeCDF(res->hdri.pixel.data(), res->hdri.numComponents,
            res->hdri.width, res->hdri.height,
            res->cumulatedRows, res->cumulatedLastCol);

#if DUMP_CDF
    HDRI dump;
    dump.width = res->hdri.width;
    dump.height = res->hdri.height;
    dump.numComponents = res->hdri.numComponents;
    dump.pixel.resize(res->hdri.width*res->hdri.height*dump.numComponents);
    for (int y = 0; y < res->hdri.height; ++y)
    {
      for (int x = 0; x < res->hdri.width; ++x)
      {
        float val = res->cumulatedRows[y * res->hdri.width + x];
        dump.pixel[(y * res->hdri.width + x) * dump.numComponents] = val;
        dump.pixel[(y * res->hdri.width + x) * dump.numComponents + 1] = val;
        dump.pixel[(y * res->hdri.width + x) * dump.numComponents + 2] = val;
        if (dump.numComponents == 4)
          dump.pixel[(y * res->hdri.width + x) * dump.numComponents + 3] = val;
      }
    }
    dump.save("cdf.hdr");
#endif

    return res;
  }

  static void tessellateRow(Geometry::SP &geom, int &idx, const std::vector<int> &xvals, int width, const float *row, float y)
  {
    geom->vertex.push_back({(float)xvals[0],y,0.f});
    geom->vertex.push_back({(float)xvals[0],y+1,0.f});
    float prev = 0.f;

    for (int x = 1; x < xvals.size(); ++x)
    {
      float curr = xvals[x]==width ? 1.f : row[xvals[x]];

      geom->vertex.push_back({(float)xvals[x],y,curr});
      geom->vertex.push_back({(float)xvals[x],y+1,curr});
      geom->index.push_back({idx,idx+1,idx+3});
      geom->index.push_back({idx,idx+3,idx+2});
      geom->slopes.push_back((curr - prev) / (xvals[x] - xvals[x-1]));
      geom->slopes.push_back((curr - prev) / (xvals[x] - xvals[x-1]));

      idx += 2;
      prev = curr;
    }
  }

  struct Point { int x; int y; };

  static std::vector<Point> generateControlPoints(
    const float *rowCdfs, const float *lastColCdf, int width, int height, const float *rowHeights,
    unsigned maxControlPoints)
  {
    struct Sample {
      Point pos;
      // Data value (not used (yet?))
      float f;
      // 1st-order forward partial derivative in x
      float dfdx;
      // 2nd-order forward partial derivative in x
      float d2fdx;
    };

    std::vector<Point> points;
    std::vector<Sample> samples((width-2) * height + (height-2));

    for (int y=0; y<height; ++y) {
      float rowHeight = (y == 0) ? lastColCdf[0] : lastColCdf[y] - lastColCdf[y-1];
      const float *row = rowCdfs + y*width;
      float prevSlope = row[1]-row[0];
      for (int x = 1; x < width-1; ++x) {
        size_t index = y * (width-2) + (x-1);
        float slope = row[x+1]-row[x];

        samples[index].pos   = {x,y};
        samples[index].f     = row[x];
        samples[index].dfdx  = slope*rowHeight;
        samples[index].d2fdx = fabsf(slope-prevSlope)*rowHeight;

        prevSlope = slope;
      }
    }

    // Same for only the last column
    size_t offset = (width-2) * height;
    {
      float rowHeight = 1.f;
      const float *row = lastColCdf;
      float prevSlope = row[1]-row[0];
      for (int x = 1; x < height-1; ++x) {
        size_t index = offset + (x-1);
        float slope = row[x+1]-row[x];

        samples[index].pos   = {x,height};
        samples[index].f     = row[x];
        samples[index].dfdx  = slope*rowHeight;
        samples[index].d2fdx = fabsf(slope-prevSlope)*rowHeight;

        prevSlope = slope;
      }
    }

    // Start by differences in slope, in descending order
    std::sort(samples.begin(),samples.end(),
              [](Sample a, Sample b) { return a.d2fdx > b.d2fdx; });

    for (int y=0; y<height; ++y)
    {
      points.push_back({0,y});
      points.push_back({width,y});
    }

    points.push_back({0,height});
    points.push_back({height,height});

    for (int i = 0; i < min((size_t)maxControlPoints,samples.size()); ++i)
    {
      if (samples[i].dfdx < FLT_MIN || samples[i].d2fdx < FLT_MIN)
        continue;

      points.push_back(samples[i].pos);
    }

    std::sort(points.begin(),points.end(),
              [](Point a, Point b) { return a.x < b.x; });

    std::stable_sort(points.begin(),points.end(),
                     [](Point a, Point b) { return a.y < b.y; });

    return points;
  }

  Mesh::SP CDF::asTriangleMesh(Mesh::Representation repr,
                               unsigned maxControlPoints,
                               bool dumpAsObj) {
    Mesh::SP res = std::make_shared<Mesh>();

    Geometry::SP geom = std::make_shared<Geometry>();

    bool rowsInSeparateBLASs = true;


    std::vector<float> rowHeights(hdri.height);
    for (int y = 0; y < hdri.height; ++y)
    {
      rowHeights[y] = (y == 0) ? cumulatedLastCol[0] : cumulatedLastCol[y] - cumulatedLastCol[y-1];
    }

    int idx=0;
    std::vector<Point> points = generateControlPoints(cumulatedRows.data(),
                                                      cumulatedLastCol.data(),
                                                      hdri.width, hdri.height,
                                                      rowHeights.data(),
                                                      maxControlPoints);
    std::vector<int> counts(hdri.height + 2);
    counts[0] = 0;

    for (unsigned i=0; i<points.size(); ++i)
    {
      counts[points[i].y+1]++;
    }

    for (unsigned i=1; i<counts.size(); ++i)
    {
      counts[i] = counts[i-1]+counts[i];
    }

    for (int y = 0; y < hdri.height; ++y)
    {
      if (rowsInSeparateBLASs && y > 0
           && !geom->vertex.empty()) // could be empty when we merged earlier
      {
        res->geoms.push_back(geom);
        geom.reset(new Geometry);
        geom->tag = Geometry::CDFRow;
        idx = 0;
      }
      int first = counts[y];
      int last = counts[y+1];
      std::vector<int> xvals;
      for (unsigned i=first; i != last; ++i)
      {
        xvals.push_back(points[i].x);
      }
      tessellateRow(geom, idx, xvals, hdri.width, cumulatedRows.data() + y * hdri.width, y);
      // Try to merge exact same neighboring rows
      if (rowsInSeparateBLASs && y > 0) {
        Geometry::SP prev = res->geoms.back();
        if (geom->vertex.size()==prev->vertex.size()) {
          bool same = true;
          for (std::size_t i = 0; i < geom->vertex.size(); ++i)
          {
            if (geom->vertex[i].x != prev->vertex[i].x
             || geom->vertex[i].z != prev->vertex[i].z)
            {
              same = false;
              break;
            }
          }

          if (same) {
            for (std::size_t i = 0; i < geom->vertex.size(); ++i)
            {
              if (fabsf(prev->vertex[i].y-y)<1e-20f) {
                prev->vertex[i].y += 1.f;
              }
            }
            geom->vertex.clear();
            geom->index.clear();
            geom->slopes.clear();
            idx = 0;
          }
        }
      }
    }

    

    if (true/*lastColInSeparateBLAS, we always do that!*/) {
      res->geoms.push_back(geom);
      geom.reset(new Geometry);
      geom->tag = Geometry::CDFLastCol;
      idx = 0;
    }
    int first = counts[hdri.height];
    int last = counts[hdri.height+1];
    std::vector<int> xvals;
    for (unsigned i=first; i != last; ++i)
    {
      xvals.push_back(points[i].x);
    }
    tessellateRow(geom, idx, xvals, hdri.height, cumulatedLastCol.data(), hdri.height);

    res->geoms.push_back(geom);

    // Normalize to obtain anisotropic bounds
    {
      for (std::size_t g = 0; g < res->geoms.size(); ++g)
      {
        Geometry::SP &geom = res->geoms[g];
        for (auto &v : geom->vertex)
        {
          v.x /= hdri.width;
          v.y /= hdri.height;
        }
      }
    }

    if (dumpAsObj) {
      std::cout << "Dumping obj...\n";
      std::stringstream str;
      size_t vertexOffset = 0;
      for (std::size_t g = 0; g < res->geoms.size(); ++g)
      {
        std::cout<<"\r" << g << "/" << res->geoms.size();
        Geometry::SP &geom = res->geoms[g];

        for (auto v : geom->vertex)
        {
          str << "v " << v.x << ' ' << v.y << ' ' << v.z << '\n';
        }
        str << "g " << g << '\n';
        for (auto i : geom->index)
        {
          vec3i index = i + vec3i(1) + vec3i((int)vertexOffset);
          str << "f " << index.x << ' ' << index.y << ' ' << index.z << '\n';
        }
        vertexOffset += geom->vertex.size();
      }
      std::string s = str.str();
      std::ofstream objFile("dump.obj");
      objFile.write(s.c_str(), s.length());
      std::cout << " Done!\n";
    }

    return res;
  }
}
