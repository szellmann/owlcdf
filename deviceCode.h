// ======================================================================== //
// Copyright 2020-2020 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "owl/owl.h"
#include "owl/common/math/vec.h"
#include "Renderer.h"
#include <cuda_runtime.h>

namespace cdf {
  
  using namespace owl;
  using namespace owl::common;
 
  struct RayGen {
  };

  struct TriangleGeom {
    vec3f *vertexBuffer;
    vec3i *indexBuffer;
    float *slopes;
  };

  struct LaunchParams
  {
    uint32_t *fbPointer;
    float4   *accumBuffer;
    int       accumID;
    struct {
      vec3f org;
      vec3f dir_00;
      vec3f dir_du;
      vec3f dir_dv;
    } camera;
    // 3D model used in rendering mode
    struct {
      vec3f *vertexBuffer;
      vec3f *indexBuffer;
      OptixTraversableHandle group;
    } model;
    // Rendering
    int renderMode;
    // Benchmarks
    int benchmarkMode;
    int *offSamples; // [0]: off-by-one, [1]: off-by-two, ...
    int offSamplesMax;
    int *offLuminance; // 12 values, off by < 1e-30,1e-20,1e-15,1e-10,1e-8,1e-7,1e-6,1e-5,1e-4,1e-3,1e-2,>=1e-2
    float luminanceMin;
    float luminanceMax;
    // hdr texture
    cudaTextureObject_t hdrTexture;
    // conventional cdf sampling
    float *environmentMapRows;
    float *environmentMapCols;
    int environmentMapWidth;
    int environmentMapHeight;
    // cdf sampling w/ triangle bvh
    OptixTraversableHandle cdfRows;
    OptixTraversableHandle cdfLastCol;
    vec3f *vertexBuffer; // TODO: is TriangleGeom not available in raygen?
    vec3i *indexBuffer;
    struct {
      int   heatMapEnabled;
      float heatMapScale;
      int   spp;
    } render;
  };
  
}
