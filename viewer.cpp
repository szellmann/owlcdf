// ======================================================================== //
// Copyright 2020-2020 The Authors                                          //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "Renderer.h"

#include "samples/common/owlViewer/OWLViewer.h"

// #include "glutViewer/OWLViewer.h"
// #include "owlViewer/OWLViewer.h"
// #include <GL/glui.h>
// #include <GL/glui/TransferFunction.h>
// #include "owlViewer/OWLViewer.h"
#include <fstream>
#include <sstream>
// #include "ColorMapper.h"

namespace cdf {
  using owl::viewer::SimpleCamera;

  const int XF_ALPHA_COUNT = 128;
  
  struct {
    std::string objFileName = "";
    std::string outFileName = "owlCDF.png";
    struct {
      vec3f vp = vec3f(0.f);
      vec3f vu = vec3f(0.f);
      vec3f vi = vec3f(0.f);
      float fovy = 70;
    } camera;
    float dt = .5f;
    vec2i windowSize = vec2i(800,600);
    std::string benchmarkMode = "none";
    int screenshotFrameNum = 0;
  } cmdline;
  
  void usage(const std::string &err)
  {
    if (err != "")
      std::cout << OWL_TERMINAL_RED << "\nFatal error: " << err
                << OWL_TERMINAL_DEFAULT << std::endl << std::endl;

    std::cout << "Usage: ./owlCDF environmentMap.{exr|hdr}" << std::endl;
    std::cout << std::endl;
    exit(1);
  }
  
  struct Viewer : public owl::viewer::OWLViewer {
  public:
    typedef owl::viewer::OWLViewer inherited;
    
    Viewer(Renderer *renderer)
      : inherited("owlCDF Sample Viewer", cmdline.windowSize),
        renderer(renderer)
    {
    }
    
    /*! this function gets called whenever the viewer widget changes
      camera settings */
    void cameraChanged() override;
    void resize(const vec2i &newSize) override;
    /*! gets called whenever the viewer needs us to re-render out widget */
    void render() override;

    /*! this gets called when the user presses a key on the keyboard ... */
    void key(char key, const vec2i &where) override
    {
      inherited::key(key,where);
      renderer->resetAccum();
      switch (key) {
      case '!':
        std::cout << "saving screenshot to 'owlDVR.png'" << std::endl;
        screenShot("owlDVR.png");
        break;
      case 'H':
        renderer->heatMapEnabled = !renderer->heatMapEnabled;
        break;
      case '<':
        renderer->heatMapScale /= 1.5f;
        break;
      case '>':
        renderer->heatMapScale *= 1.5f;
        break;
      case ')':
        renderer->spp++;
        PRINT(renderer->spp);
        break;
      case '(':
        renderer->spp = max(1,renderer->spp-1);
        PRINT(renderer->spp);
        break;
      }
    }

    
  public:

    Renderer *const renderer;
  };
  

  void Viewer::resize(const vec2i &newSize) 
  {
    // ... tell parent to resize (also resizes the pbo in the wingdow)
    inherited::resize(newSize);
    cameraChanged();
    renderer->resetAccum();
  }
    
  /*! this function gets called whenever the viewer widget changes
    camera settings */
  void Viewer::cameraChanged() 
  {
    inherited::cameraChanged();
    const SimpleCamera &camera = inherited::getCamera();
    
    const vec3f screen_du = camera.screen.horizontal / float(getWindowSize().x);
    const vec3f screen_dv = camera.screen.vertical   / float(getWindowSize().y);
    const vec3f screen_00 = camera.screen.lower_left;
    renderer->setCamera(camera.lens.center,screen_00,screen_du,screen_dv);
    renderer->resetAccum();
  }
    

  /*! gets called whenever the viewer needs us to re-render out widget */
  void Viewer::render() 
  {
    static double t_last = getCurrentTime();
    static double t_first = t_last;

    renderer->render(fbSize,fbPointer);
      
    double t_now = getCurrentTime();
    static double avg_t = t_now-t_last;
    // if (t_last >= 0)
    avg_t = 0.8*avg_t + 0.2*(t_now-t_last);

    size_t numRays = fbSize.x*fbSize.y*Renderer::spp*2;
    double numRaysPerSec = numRays/avg_t;

    static int counter = 0;
    if (counter++ % 10 == 0)
      std::cout << "Realizing " << prettyNumber(numRaysPerSec) << "ops/sec\n";

    if (counter == cmdline.screenshotFrameNum) {
      std::stringstream str;
      str<<"spp"<<cmdline.screenshotFrameNum<<"cp"<<Renderer::maxControlPoints<<".png";
      std::string s = str.str();
      screenShot(s.c_str());
    }

    char title[1000];
    sprintf(title,"mowlana - %.2f FPS",(1.f/avg_t));
    setTitle(title);
    // setWindowTitle(title);
    // glfwSetWindowTitle(this->handle,title);

    t_last = t_now;
  }

  extern "C" int main(int argc, char **argv)
  {
    std::string inFileName;

    // Viewer::initGlut(argc,argv);
    
    for (int i=1;i<argc;i++) {
      const std::string arg = argv[i];
      if (arg[0] != '-') {
        inFileName = arg;
      } else if (arg == "--obj" || arg == "-obj") {
        cmdline.objFileName = argv[++i];
      } else if (arg == "-fovy") {
        cmdline.camera.fovy = std::stof(argv[++i]);
      }
      else if (arg == "-win") {
        cmdline.windowSize.x = std::stoi(argv[++i]);
        cmdline.windowSize.y = std::stoi(argv[++i]);
      }
      else if (arg == "--camera") {
        cmdline.camera.vp.x = std::stof(argv[++i]);
        cmdline.camera.vp.y = std::stof(argv[++i]);
        cmdline.camera.vp.z = std::stof(argv[++i]);
        cmdline.camera.vi.x = std::stof(argv[++i]);
        cmdline.camera.vi.y = std::stof(argv[++i]);
        cmdline.camera.vi.z = std::stof(argv[++i]);
        cmdline.camera.vu.x = std::stof(argv[++i]);
        cmdline.camera.vu.y = std::stof(argv[++i]);
        cmdline.camera.vu.z = std::stof(argv[++i]);
      }
      else if (arg == "-win"  || arg == "--win" || arg == "--size") {
        cmdline.windowSize.x = std::atoi(argv[++i]);
        cmdline.windowSize.y = std::atoi(argv[++i]);
      }
      else if (arg == "-o") {
        cmdline.outFileName = argv[++i];
      }
      else if (arg == "-spp" || arg == "--spp") {
        Renderer::spp = std::stoi(argv[++i]);
      }
      else if (arg == "--heat-map") {
        Renderer::heatMapEnabled = true;
        Renderer::heatMapScale = std::stof(argv[++i]);
      } else if (arg == "-rm" || arg == "--rendermode") {
        std::string val(argv[++i]);
        if (val == "bvh")
          Renderer::renderMode = RenderModeBVH;
        else if(val == "binary")
          Renderer::renderMode = RenderModeBinarySearch;
        else
          usage("invalid value for argument --rendermode: '"+val+"'");
      } else if (arg == "--benchmark" || arg == "-bench") {
        std::string val(argv[++i]);
        if (val == "bvh")
          Renderer::benchmarkMode = BenchmarkModeBVH;
        else if (val == "binary")
          Renderer::benchmarkMode = BenchmarkModeBinarySearch;
        else if (val == "errors")
          Renderer::benchmarkMode = BenchmarkModeErrors;
        else if (val == "none") // default
          Renderer::benchmarkMode = BenchmarkModeNone;
        else
          usage("invalid value for argument --benchmark: '"+val+"'");
      }  else if (arg == "-dumpObj" || arg == "--dumpObj") {
        Renderer::cdfDumpAsObj = true;
      } else if (arg == "-screenshotfn" || arg == "--screenshotFrameNum") {
        cmdline.screenshotFrameNum = std::stoi(argv[++i]);
      }  else if (arg == "-quads" || arg == "--max_quads-per-ribbon"
              || arg == "-cp"    || arg == "--max-control-points") {
        Renderer::maxControlPoints = std::stoi(argv[++i]);
      }
      else
        usage("unknown cmdline arg '"+arg+"'");
    }
    
    if (inFileName == "")
      usage("no filename specified");

    Renderer renderer(inFileName,cmdline.objFileName);
 
    const box3f modelBounds = renderer.modelBounds;

    Viewer *viewer = new Viewer(&renderer);
    // viewer->resize(QSize(128,128));
    // if (cmdline.windowSize != vec2i(0))
    //   viewer.setWindowSize(cmdline.windowSize);

    viewer->enableFlyMode();
    viewer->enableInspectMode(/* valid range of poi*/modelBounds,
                              /* min distance      */1e-3f,
                              /* max distance      */1e8f);

    if (cmdline.camera.vu != vec3f(0.f)) {
      viewer->setCameraOrientation(/*origin   */cmdline.camera.vp,
                                   /*lookat   */cmdline.camera.vi,
                                   /*up-vector*/cmdline.camera.vu,
                                   /*fovy(deg)*/cmdline.camera.fovy);
    } else {
      viewer->setCameraOrientation(/*origin   */
                                   modelBounds.center()
                                   + vec3f(-.3f, .7f, +1.f) * modelBounds.span(),
                                   /*lookat   */modelBounds.center(),
                                   /*up-vector*/vec3f(0.f, 1.f, 0.f),
                                   /*fovy(deg)*/70.f);
    }
    viewer->setWorldScale(10.1f*length(modelBounds.span()));

    

    viewer->showAndRun();
  }
  
}

